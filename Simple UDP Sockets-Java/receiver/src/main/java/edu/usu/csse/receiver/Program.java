package edu.usu.csse.receiver;

public class Program {
    public static void main(String[] args) {
        SimpleReceiver receiver = new SimpleReceiver();
        if (receiver.Initialize())
            receiver.doStuff();
    }
}
