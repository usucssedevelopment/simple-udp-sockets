package edu.usu.csse.receiver;

import edu.usu.csse.common.BaseSenderReceiver;
import edu.usu.csse.common.Message;
import edu.usu.csse.common.NetworkUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.List;

public class SimpleReceiver extends BaseSenderReceiver {
    private static Logger log = LogManager.getFormatterLogger(SimpleReceiver.class.getName());

    public void doStuff()
    {
        DisplayEndPoints();

        System.out.println("");
        System.out.println("Receiving...");
        boolean quit = false;

        while (!quit)
        {
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            try
            {
                SocketAddress remoteEndPoint = myUdpClient.receive(buffer);
                if (remoteEndPoint == null) {
                    Thread.sleep(0);
                    continue;
                }

                buffer.flip();
                quit = DecodeAndDisplayMessage((InetSocketAddress) remoteEndPoint, buffer);
            }
            catch (IOException ex)
            {
                log.error("Cannot local end point for UDP Client: %s", ex.getMessage());
            } catch (InterruptedException ex) {
                log.error("Unexpected problem with sleep: %s", ex.getMessage());
            }

        }
    }

    private void DisplayEndPoints()
    {
        InetSocketAddress localEndPoint = getLocalEndPoint();
        if (localEndPoint==null) return;

        System.out.println("");
        System.out.println("Receiver accepting messages at the following end point(s):");

        if (!localEndPoint.getAddress().isLoopbackAddress())
        {
            List<String> localAddresses = NetworkUtils.getIpAddresses();
            for (String address : localAddresses)
                System.out.println("\t" + address + ":" + localEndPoint.getPort());
        }
        else
            System.out.println("\t"+localEndPoint);
    }

    private static boolean DecodeAndDisplayMessage(InetSocketAddress remoteEndPoint, ByteBuffer bytes)
    {
        if (bytes == null) return false;

        Message message = Message.decode(bytes);
        if (message == null) return false;

        System.out.println("Received Message " + message.getId() + " sent at " + message.getTimestamp() +
                " from " + remoteEndPoint + " --> " + message.getText());
        return (message.getText().trim().toUpperCase() == "EXIT");
    }

}
