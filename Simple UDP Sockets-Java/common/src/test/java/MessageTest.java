import edu.usu.csse.common.Message;
import org.junit.Test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Date;

import static org.junit.Assert.*;

public class MessageTest {
    @Test
    public void checkConstructionAndGetters() {
        Date timestamp = new Date();

        Message msg = new Message(1, timestamp, "ABC");
        assertEquals(1, msg.getId());
        assertEquals(timestamp, msg.getTimestamp());
        assertEquals("ABC", msg.getText());

        msg.setId(2);
        assertEquals(2, msg.getId());

        Date timestamp2 = new Date(timestamp.getTime()-60000);

        msg.setTimestamp(timestamp2);
        assertEquals(timestamp2, msg.getTimestamp());

        msg.setText("Hello");
        assertEquals("Hello", msg.getText());
    }

    @Test
    public void checkEncoding() throws IOException {

        Date timestamp = new Date();

        Message msg = new Message(1, timestamp, "ABC");
        ByteBuffer bytes = msg.encode();

        assertEquals(0, bytes.get(0));
        assertEquals(0, bytes.get(1));
        assertEquals(0, bytes.get(2));
        assertEquals(1, bytes.get(3));

        Message msg2 = Message.decode(bytes);
        assertNotNull(msg2);
        assertEquals(msg.getId(), msg2.getId());
        assertEquals(msg.getTimestamp(), msg2.getTimestamp());
        assertEquals(msg.getText(), msg2.getText());

    }

}