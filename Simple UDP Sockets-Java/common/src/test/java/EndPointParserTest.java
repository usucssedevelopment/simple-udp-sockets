import edu.usu.csse.common.EndPointParser;
import org.junit.Test;

import java.net.InetSocketAddress;

import static org.junit.Assert.*;

public class EndPointParserTest {

    @Test
    public void checkParse() {
        InetSocketAddress ep = EndPointParser.Parse(null);
        assertNull(ep);

        ep = EndPointParser.Parse("");
        assertNull(ep);

        ep = EndPointParser.Parse(" :  ");
        assertNull(ep);

        ep = EndPointParser.Parse("127.0.0.1:  ");
        assertNull(ep);

        ep = EndPointParser.Parse(":2300");
        assertNull(ep);

        ep = EndPointParser.Parse("127.0.0.1:2300");
        assertNotNull(ep);
        assertEquals("127.0.0.1", ep.getAddress().getHostAddress().toString());
        assertEquals(2300, ep.getPort());

        ep = EndPointParser.Parse("www.usu.edu:2300");
        assertNotNull(ep);
        assertTrue(ep.getAddress().getHostAddress().toString().startsWith("129.123"));
        assertEquals(2300, ep.getPort());

    }
}