import edu.usu.csse.common.NetworkUtils;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class NetworkUtilsTest {

    @Test
    public void testGetIpAddresses() {
        List<String> addresses = NetworkUtils.getIpAddresses();
        assertNotNull(addresses);
        assertTrue(addresses.contains("127.0.0.1"));
    }
}