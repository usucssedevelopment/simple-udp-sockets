package edu.usu.csse.common;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Message {
    private static Logger log = LogManager.getFormatterLogger(Message.class.getName());

    private int id;
    private Date timestamp;
    private String text;
    private ByteArrayOutputStream outputStream;

    public Message(int id, Date timestamp, String text) {
        this.id = id;
        this.timestamp = timestamp;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static Message decode(ByteBuffer bytes) {
        log.debug("Entering decode");

        if (bytes==null || bytes.remaining()< Integer.BYTES + Long.BYTES + Short.BYTES) {
            log.warn("Byte array is either null or too short");
            return null;
        }

        log.debug("Byte buffer contains %d bytes", bytes.remaining());

        bytes.order(ByteOrder.BIG_ENDIAN);

        int id = decodeInt(bytes);
        Date timestamp = new Date(decodeLong(bytes));
        String text = decodeString(bytes);

        Message message = new Message(id, timestamp, text);

        log.debug("Leaving decode, message.id=%d, message.timestamp=%s, message.text=%s",
                        message.getId(), message.getTimestamp(), message.getText());

        return message;
    }

    public ByteBuffer encode() throws IOException {
        log.debug("Entering encode");

        outputStream = new ByteArrayOutputStream();
        encodeInt(id);
        encodeLong(timestamp.getTime());
        encodeString(text);

        log.debug("encoded %d bytes",outputStream.size());
        return ByteBuffer.wrap(outputStream.toByteArray());
    }

    private void encodeShort(short value) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES);
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.putShort(value);
        outputStream.write(buffer.array());
    }

    private void encodeInt(int value) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.putInt(value);
        outputStream.write(buffer.array());
    }

    private void encodeLong(long value) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.putLong(value);
        outputStream.write(buffer.array());
    }

    private void encodeString(String value) throws IOException {
        if (value==null)
            value="";

        byte[] textBytes = value.getBytes(Charset.forName("UTF-16BE"));
        encodeShort((short) (textBytes.length));
        outputStream.write(textBytes);
    }

    private static short decodeShort(ByteBuffer bytes) {
        return bytes.getShort();
    }

    private static int decodeInt(ByteBuffer bytes) {
        return bytes.getInt();
    }

    private static long decodeLong(ByteBuffer bytes) {
        return bytes.getLong();
    }

    private static String decodeString(ByteBuffer bytes) {
        short textLength = decodeShort(bytes);
        if (bytes.remaining() < textLength) {
            log.warn("Byte array is too short for specific text");
            return null;
        }

        log.debug("text length=%d", textLength);
        byte[] textBytes = new byte[textLength];
        bytes.get(textBytes, 0, textLength);
        return new String(textBytes, Charset.forName("UTF-16BE"));
    }

}
