package edu.usu.csse.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.DatagramChannel;

public class BaseSenderReceiver {
    private static Logger log = LogManager.getFormatterLogger(BaseSenderReceiver.class.getName());

    protected DatagramChannel myUdpClient;

    public boolean Initialize() {
        log.debug("Create UdpClient");

        boolean result = false;
        try {
            myUdpClient = DatagramChannel.open();
            InetSocketAddress localEndPoint = new InetSocketAddress("0.0.0.0", 0);
            myUdpClient.bind(localEndPoint);
            log.info("myUdpClient is bound to " + getLocalEndPoint());
            result = true;
        }
        catch (IOException ex) {
            log.error("Cannot setup UDP Client: %s", ex.getMessage());
        }
        return result;
    }

    protected InetSocketAddress getLocalEndPoint() {
        if (myUdpClient==null) return null;

        InetSocketAddress localEndPoint = null;
        try {
            localEndPoint = (InetSocketAddress) myUdpClient.getLocalAddress();
        }
        catch (IOException ex) {
            log.error("Cannot local end point for UDP Client: %s", ex.getMessage());
        }
        return localEndPoint;
    }

}
