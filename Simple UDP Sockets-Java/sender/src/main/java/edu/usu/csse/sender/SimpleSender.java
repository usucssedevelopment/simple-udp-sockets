package edu.usu.csse.sender;

import edu.usu.csse.common.BaseSenderReceiver;
import edu.usu.csse.common.EndPointParser;
import edu.usu.csse.common.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class SimpleSender extends BaseSenderReceiver {
    private static Logger log = LogManager.getFormatterLogger(SimpleSender.class.getName());
    private Scanner consoleInput;

    private int _nextId = 1;

    public List<InetSocketAddress> peers = new ArrayList<>();

    public void doStuff()
    {
        log.info("Entering doStuff");

        consoleInput = new Scanner(System.in);
        boolean keepGoing = true;

        while (keepGoing)
        {
            System.out.println("A=Add Peer, S=Send Message, or EXIT: " );
            String cmd = consoleInput.nextLine();
            if (cmd == null) continue;

            cmd = cmd.trim().toUpperCase();

            if (cmd.equals("A"))
                addPeer();
            if (cmd.equals("S"))
                askForMessageAndSend();
            if (cmd.equals("EXIT")) {
                sendToPeers(cmd);
                keepGoing = false;
            }
        }
    }

    private void addPeer()
    {
        System.out.println("Enter Peer EP (host:port): ");
        String peer = consoleInput.nextLine();
        InetSocketAddress peerAddress = EndPointParser.Parse(peer);
        if (peerAddress!=null)
            peers.add(peerAddress);
    }

    private void askForMessageAndSend()
    {
        System.out.println("Enter a message to send: ");
        String message = consoleInput.nextLine();
        if (message!=null)
            sendToPeers(message);
    }

    private void sendToPeers(String message)
    {
        if (peers == null || peers.size() <= 0) return;

        Message msg = new Message(getNextId(), new Date(), message);

        try {
            ByteBuffer bytesToSent = msg.encode();
            log.debug("Bytes to send = %s", bytesToSent.toString());

            for (InetSocketAddress ep : peers) {
                int result = myUdpClient.send(bytesToSent, ep);
                System.out.println("Sent " + result + " bytes to " + ep);
                bytesToSent.rewind();
            }
        }
        catch (IOException ex) {
            log.error("Cannot send message ");
        }
    }

    private int getNextId()
    {
        if (_nextId == Integer.MAX_VALUE)
            _nextId = 1;
        return _nextId++;
    }


}
