package edu.usu.csse.sender;

public class Program {
    public static void main(String[] args) {
        SimpleSender sender = new SimpleSender();
        if (sender.Initialize())
            sender.doStuff();
    }
}
