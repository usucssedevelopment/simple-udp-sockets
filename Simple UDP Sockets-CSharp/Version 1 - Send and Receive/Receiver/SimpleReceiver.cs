﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Common;

namespace Receiver
{
    public class SimpleReceiver
    {
        private readonly UdpClient _myUdpClient;

        public SimpleReceiver()
        {
            var localEp = new IPEndPoint(IPAddress.Any, 0);
            _myUdpClient = new UdpClient(localEp) {Client = {ReceiveTimeout = 1000}};
        }

        public IPEndPoint LocalEp
        {
            get
            {
                IPEndPoint result = null;
                if (_myUdpClient != null)
                    result = _myUdpClient.Client.LocalEndPoint as IPEndPoint;
                return result;
            }
        }

        public void DoStuff()
        {
            DisplayEndPoints();

            Console.WriteLine();
            Console.WriteLine("Receiving...");
            var message = string.Empty;
            while (message.Trim().ToUpper() != "EXIT")
            {
                var remoteEp = new IPEndPoint(IPAddress.Any, 0);
                byte[] receiveBuffer = null;
                try
                {
                    receiveBuffer = _myUdpClient.Receive(ref remoteEp);
                }
                catch (SocketException)
                {
                    // Console.WriteLine("Timeout");
                }
                if (receiveBuffer == null) continue;

                message = Encoding.BigEndianUnicode.GetString(receiveBuffer);
                Console.WriteLine($"Message from {remoteEp} --> {message}");
            }
        }

        private void DisplayEndPoints()
        {
            Console.WriteLine();
            Console.WriteLine("Receiver accepting messages at the following end point(s):");

            var localEp = _myUdpClient.Client.LocalEndPoint as IPEndPoint;
            if (localEp!=null && localEp.Address.Equals(IPAddress.Any))
            {
                var addresses = NetworkUtils.GetIpAddresses();
                foreach (var address in addresses)
                    Console.WriteLine($"\t{address}:{localEp.Port}");
            }
            else
                Console.WriteLine($"\t{localEp}");
        }
    }
}
