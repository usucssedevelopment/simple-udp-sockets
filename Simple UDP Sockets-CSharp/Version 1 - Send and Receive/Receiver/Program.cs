﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Receiver
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var receiver = new SimpleReceiver();
            receiver.DoStuff();
        }
    }
}
