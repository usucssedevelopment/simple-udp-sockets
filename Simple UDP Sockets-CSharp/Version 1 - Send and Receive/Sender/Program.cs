﻿namespace Sender
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var sender = new SimpleSender();
            sender.DoStuff();
        }
    }
}
