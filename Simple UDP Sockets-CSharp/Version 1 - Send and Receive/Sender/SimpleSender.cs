﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Common;

namespace Sender
{
    public class SimpleSender
    {
        private readonly UdpClient _myUdpClient;

        public List<IPEndPoint> Peers { get; set; }

        public SimpleSender()
        {
            var localEp = new IPEndPoint(IPAddress.Any, 0);
            _myUdpClient = new UdpClient(localEp);
            Peers = new List<IPEndPoint>();
        }

        public void DoStuff()
        {
            var cmd = string.Empty;
            while (string.IsNullOrEmpty(cmd) || cmd.Trim().ToUpper() != "EXIT")
            {
                Console.Write("A=Add Peer, S=Send Message, or EXIT: " );
                cmd = Console.ReadLine();
                if (cmd == null) continue;

                switch (cmd.Trim().ToUpper())
                {
                    case "A":
                        AddPeer();
                        break;
                    case "S":
                        AskForMessageAndSend();
                        break;
                    case "EXIT":
                        SendToPeers(cmd);
                        break;
                }
            }
        }

        private void AddPeer()
        {
            Console.Write("Enter Peer EP (host:port): ");
            var peer = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(peer)) return;

            var peerAddress = EndPointParser.Parse(peer);
            if (peerAddress!=null)
                Peers.Add(peerAddress);
        }

        private void AskForMessageAndSend()
        {
            Console.Write("Enter a message to send: ");
            var message = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(message))
                SendToPeers(message);
        }

        private void SendToPeers(string message)
        {
            if (Peers == null || Peers.Count <= 0) return;

            var sendBuffer = Encoding.BigEndianUnicode.GetBytes(message);
            foreach (var ep in Peers)
            {
                var result = _myUdpClient.Send(sendBuffer, sendBuffer.Length, ep);
                Console.WriteLine($"Sent {result} bytes");
            }
        }
    }
}
