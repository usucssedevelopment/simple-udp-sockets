﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common;

namespace CommonTest
{
    [TestClass]
    public class NetworkUtilsTest
    {
        [TestMethod]
        public void NetworkUtils_TestEverything()
        {
            var addresses = NetworkUtils.GetIpAddresses();
            Assert.IsNotNull(addresses);
            Assert.IsTrue(addresses.Contains("127.0.0.1"));
        }
    }
}
