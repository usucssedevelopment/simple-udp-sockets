﻿using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Common;

namespace CommonTest
{
    [TestClass]
    public class MessageTest
    {
        [TestMethod]
        public void Message_TestNormalEncodingAndDecoding()
        {
            var msg = new Message()
            {
                Id = 1,
                Timestamp = DateTime.Parse("2017-09-01 10:04:03"),
                Text = "ABC"
            };

            var bytes = msg.Encode();
            Assert.AreEqual(20, bytes.Length);  // 4 bytes for Id, 8 for Timestamp, 2 for Text's length, 6 for the Text in Unicode                            

            // Check encoding of Id
            Assert.AreEqual(0, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(1, bytes[3]);

            // Check encoding of Timestamp
            var ticks = msg.Timestamp.Ticks;
            var expectedTickBytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(ticks));
            for (var i=0; i<8; i++)
                Assert.AreEqual(expectedTickBytes[i], bytes[i+4]);

            // Check encoding of Text
            Assert.AreEqual(0, bytes[12]);        // String length of 6
            Assert.AreEqual(6, bytes[13]);
            Assert.AreEqual(0, bytes[14]);        // "A"
            Assert.AreEqual(65, bytes[15]);
            Assert.AreEqual(0, bytes[16]);        // "B"
            Assert.AreEqual(66, bytes[17]);
            Assert.AreEqual(0, bytes[18]);        // "C"
            Assert.AreEqual(67, bytes[19]);

            var msg2 = Message.Decode(bytes);
            Assert.AreEqual(msg.Id, msg2.Id);
            Assert.AreEqual(msg.Timestamp, msg2.Timestamp);
            Assert.AreEqual(msg.Text, msg2.Text);
        }

        [TestMethod]
        public void Message_TestNoText()
        {
            var msg = new Message()
            {
                Id = 1,
                Timestamp = DateTime.Parse("2017-09-01 10:04:03"),
                Text = ""
            };

            var bytes = msg.Encode();
            Assert.AreEqual(14, bytes.Length);  // 4 bytes for Id, 8 for Timestamp, 2 for Text's length                         

            // Check encoding of Id
            Assert.AreEqual(0, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(1, bytes[3]);

            // Check encoding of Timestamp
            var ticks = msg.Timestamp.Ticks;
            var expectedTickBytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(ticks));
            for (var i = 0; i < 8; i++)
                Assert.AreEqual(expectedTickBytes[i], bytes[i + 4]);

            // Check encoding of Text
            Assert.AreEqual(0, bytes[12]);        // String length of 0
            Assert.AreEqual(0, bytes[13]);

            var msg2 = Message.Decode(bytes);
            Assert.AreEqual(msg.Id, msg2.Id);
            Assert.AreEqual(msg.Timestamp, msg2.Timestamp);
            Assert.AreEqual(msg.Text, msg2.Text);
        }

        [TestMethod]
        public void Message_TestNullText()
        {
            var msg = new Message()
            {
                Id = 1,
                Timestamp = DateTime.Parse("2017-09-01 10:04:03"),
                Text = null 
            };

            var bytes = msg.Encode();
            Assert.AreEqual(14, bytes.Length);  // 4 bytes for Id, 8 for Timestamp, 2 for Text's length                         

            // Check encoding of Id
            Assert.AreEqual(0, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(1, bytes[3]);

            // Check encoding of Timestamp
            var ticks = msg.Timestamp.Ticks;
            var expectedTickBytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(ticks));
            for (var i = 0; i < 8; i++)
                Assert.AreEqual(expectedTickBytes[i], bytes[i + 4]);

            // Check encoding of Text
            Assert.AreEqual(0, bytes[12]);        // String length of 0
            Assert.AreEqual(0, bytes[13]);

            var msg2 = Message.Decode(bytes);
            Assert.AreEqual(msg.Id, msg2.Id);
            Assert.AreEqual(msg.Timestamp, msg2.Timestamp);
            Assert.AreEqual("", msg2.Text);
        }

        [TestMethod]
        public void Message_TestDecodingWithNoBytes()
        {
            var msg = Message.Decode(null);
            Assert.IsNull(msg);
        }

        [TestMethod]
        public void Message_TestDecodingWithToFewBytes()
        {
            var msg = new Message()
            {
                Id = 1,
                Text = "ABC",
                Timestamp = DateTime.Parse("2017-09-01 10:04:03")
            };
            var bytes = msg.Encode();

            for (var badLength = 0; badLength < bytes.Length - 1; badLength++)
            {
                var badBytes = new byte[badLength];
                for (var i = 0; i < badLength; i++)
                    badBytes[i] = bytes[i];

                try
                {
                    var msg2 = Message.Decode(badBytes);
                    Assert.Fail("Expected an exception to be thrown");
                }
                catch (ApplicationException)
                {
                    // Ignore
                }
            }
        }
    }
}
