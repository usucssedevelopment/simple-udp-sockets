﻿using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common;

namespace CommonTest
{
    [TestClass]
    public class EndPointParserTest
    {
        [TestMethod]
        public void EndPointParser_CheckParse()
        {
            var ep = EndPointParser.Parse(null);
            Assert.IsNull(ep);

            ep = EndPointParser.Parse("");
            Assert.IsNull(ep);

            ep = EndPointParser.Parse(" :  ");
            Assert.IsNull(ep);

            ep = EndPointParser.Parse("127.0.0.1:  ");
            Assert.IsNull(ep);

            ep = EndPointParser.Parse(":2300");
            Assert.IsNull(ep);

            ep = EndPointParser.Parse("127.0.0.1:2300");
            Assert.IsNotNull(ep);
            Assert.AreEqual("127.0.0.1", ep.Address.ToString());
            Assert.AreEqual(2300, ep.Port);

            ep = EndPointParser.Parse("127.0.0.1:123:2300");
            Assert.IsNull(ep);

            ep = EndPointParser.Parse("www.usu.edu:2300");
            Assert.IsNotNull(ep);
            Assert.IsTrue(ep.Address.ToString().StartsWith("129.123"));
            Assert.AreEqual(2300, ep.Port);

        }

    }
}
