﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Common;
using log4net;

namespace Sender
{
    public class SimpleSender
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SimpleSender));

        private int _nextId = 1;

        private readonly UdpClient _myUdpClient;

        private readonly List<IPEndPoint> _peers = new List<IPEndPoint>();

        public SimpleSender()
        {
            var localEp = new IPEndPoint(IPAddress.Any, 0);
            _myUdpClient = new UdpClient(localEp);
        }

        public void DoStuff()
        {
            var keepGoing = true;

            while (keepGoing)
            {
                Console.Write("A=Add Peer, S=Send Message, or EXIT: " );
                var cmd = Console.ReadLine();
                if (cmd == null) continue;

                switch (cmd.Trim().ToUpper())
                {
                    case "A":
                        AddPeer();
                        break;
                    case "S":
                        AskForMessageAndSend();
                        break;
                    case "EXIT":
                        SendToPeers(cmd);
                        keepGoing = false;
                        break;
                }
            }
        }

        private void AddPeer()
        {
            Console.Write("Enter Peer EP (host:port): ");
            var peer = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(peer)) return;

            var peerAddress = EndPointParser.Parse(peer);
            if (peerAddress!=null)
                _peers.Add(peerAddress);
        }

        private void AskForMessageAndSend()
        {
            Console.Write("Enter a message to send: ");
            var message = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(message))
                SendToPeers(message);
        }

        private void SendToPeers(string message)
        {
            if (_peers == null || _peers.Count <= 0) return;

            var msg = new Message()
            {
                Id = GetNextId(),
                Timestamp = DateTime.Now,
                Text = message
            };

            var bytesToSent = msg.Encode();

            foreach (var ep in _peers)
            {
                var result = _myUdpClient.Send(bytesToSent, bytesToSent.Length, ep);
                Logger.Debug($"Sent {result} bytes to {ep}");
            }
        }

        private int GetNextId()
        {
            if (_nextId == int.MaxValue)
                _nextId = 1;
            return _nextId++;
        }
    }
}
