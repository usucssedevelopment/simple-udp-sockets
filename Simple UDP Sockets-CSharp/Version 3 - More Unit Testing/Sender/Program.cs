﻿using log4net.Config;

namespace Sender
{
    public class Program
    {
        public static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            var sender = new SimpleSender();
            sender.DoStuff();
        }
    }
}
