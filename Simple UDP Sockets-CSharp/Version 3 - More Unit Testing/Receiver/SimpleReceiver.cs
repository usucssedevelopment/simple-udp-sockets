﻿using System;
using System.Net;
using System.Net.Sockets;
using Common;
using log4net;

namespace Receiver
{
    public class SimpleReceiver
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SimpleReceiver));

        private readonly UdpClient _myUdpClient;

        public SimpleReceiver()
        {
            Logger.Debug("Create UdpClient");
            var localEp = new IPEndPoint(IPAddress.Any, 0);
            _myUdpClient = new UdpClient(localEp) {Client = {ReceiveTimeout = 1000}};
        }

        public IPEndPoint LocalEp
        {
            get
            {
                IPEndPoint result = null;
                if (_myUdpClient != null)
                    result = _myUdpClient.Client.LocalEndPoint as IPEndPoint;
                return result;
            }
        }

        public void DoStuff()
        {
            DisplayEndPoints();

            Console.WriteLine();
            Console.WriteLine("Receiving...");
            var quit = false;
            while (!quit)
            {
                IPEndPoint remoteEp = new IPEndPoint(IPAddress.Any, 0);
                byte[] bytes = null;

                try
                {
                    bytes = _myUdpClient.Receive(ref remoteEp);
                }
                catch (SocketException err)
                {
                    if (err.SocketErrorCode != SocketError.TimedOut)
                        throw;
                }

                if (bytes != null)
                    quit = DecodeAndDisplayMessage(remoteEp, bytes);
            }
        }

        private void DisplayEndPoints()
        {
            Console.WriteLine();
            Console.WriteLine("Receiver accepting messages at the following end point(s):");

            var localEp = _myUdpClient.Client.LocalEndPoint as IPEndPoint;
            if (localEp!=null && localEp.Address.Equals(IPAddress.Any))
            {
                var addresses = NetworkUtils.GetIpAddresses();
                foreach (var address in addresses)
                    Console.WriteLine($"\t{address}:{localEp.Port}");
            }
            else
                Console.WriteLine($"\t{localEp}");
        }

        private static bool DecodeAndDisplayMessage(IPEndPoint remoteEp, byte[] bytes)
        {
            if (bytes == null) return false;

            var message = Message.Decode(bytes);
            if (message == null) return false;

            Console.WriteLine($"Received Message {message.Id} sent at {message.Timestamp} from {remoteEp} --> {message.Text}");
            return (message.Text.Trim().ToUpper() == "EXIT");
        }
    }
}
