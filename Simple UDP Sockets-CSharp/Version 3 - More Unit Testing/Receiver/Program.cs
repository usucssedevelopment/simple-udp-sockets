﻿using log4net.Config;

namespace Receiver
{
    public class Program
    {
        public static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            var receiver = new SimpleReceiver();
            receiver.DoStuff();
        }
    }
}
