﻿using System;
using System.IO;
using System.Net;
using System.Text;
using log4net;

namespace Common
{
    public class Message
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (Message));

        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Text { get; set; }

        public byte[] Encode()
        {
            Logger.Debug("Encode message");

            var memoryStream = new MemoryStream();

            Write(memoryStream, Id);
            Write(memoryStream, Timestamp.Ticks);
            Write(memoryStream, Text);

            return memoryStream.ToArray();
        }

        public static Message Decode(byte[] bytes)
        {
            if (bytes == null) return null;

            var message = new Message();

            var memoryStream = new MemoryStream(bytes);

            message.Id = ReadInt(memoryStream);
            var ticks = ReadLong(memoryStream);
            message.Timestamp = new DateTime(ticks);
            message.Text = ReadString(memoryStream);

            return message;
        }

        private static void Write(Stream memoryStream, short value)
        {
            var bytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value));
            memoryStream.Write(bytes, 0, bytes.Length);
        }

        private static void Write(Stream memoryStream, int value)
        {
            var bytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value));
            memoryStream.Write(bytes, 0, bytes.Length);
        }

        private static void Write(Stream memoryStream, long value)
        {
            var bytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value));
            memoryStream.Write(bytes, 0, bytes.Length);
        }

        private static void Write(Stream memoryStream, string value)
        {
            if (value==null)
                value = string.Empty;

            var bytes = Encoding.BigEndianUnicode.GetBytes(value);
            Write(memoryStream, (short) bytes.Length);
            memoryStream.Write(bytes, 0, bytes.Length);
        }

        private static short ReadShort(Stream memoryStream)
        {
            var bytes = new byte[2];
            var bytesRead = memoryStream.Read(bytes, 0, bytes.Length);
            if (bytesRead != bytes.Length)
                throw new ApplicationException("Cannot decode a short integer from message");

            return IPAddress.NetworkToHostOrder(BitConverter.ToInt16(bytes, 0));
        }

        private static int ReadInt(Stream memoryStream)
        {
            var bytes = new byte[4];
            var bytesRead = memoryStream.Read(bytes, 0, bytes.Length);
            if (bytesRead != bytes.Length)
                throw new ApplicationException("Cannot decode an integer from message");

            return IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes, 0));
        }

        private static long ReadLong(Stream memoryStream)
        {
            var bytes = new byte[8];
            var bytesRead = memoryStream.Read(bytes, 0, bytes.Length);
            if (bytesRead != bytes.Length)
                throw new ApplicationException("Cannot decode a long integer from message");

            return IPAddress.NetworkToHostOrder(BitConverter.ToInt64(bytes, 0));
        }

        private static string ReadString(Stream memoryStream)
        {
            var result = string.Empty;
            var length = ReadShort(memoryStream);
            if (length <= 0) return result;

            var bytes = new byte[length];
            var bytesRead = memoryStream.Read(bytes, 0, bytes.Length);
            if (bytesRead != length)
                throw new ApplicationException("Cannot decode a string from message");

            result = Encoding.BigEndianUnicode.GetString(bytes, 0, bytes.Length);
            return result;
        }
    }
}
