﻿using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Common
{
    public static class NetworkUtils
    {
        public static List<string> GetIpAddresses()
        {
            var addresses = new List<string>();
            var adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (var adapter in adapters)
            {
                if (!adapter.Supports(NetworkInterfaceComponent.IPv4)) continue;

                var adapterProperties = adapter.GetIPProperties();
                var uniCastAddresses = adapterProperties.UnicastAddresses;
                if (uniCastAddresses == null) continue;

                foreach (var addressInformation in uniCastAddresses)
                    if (addressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                        addresses.Add(addressInformation.Address.ToString());
            }

            return addresses;
        }
    }
}
