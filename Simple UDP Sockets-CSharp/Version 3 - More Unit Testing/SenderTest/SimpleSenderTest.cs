﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sender;

namespace SenderTest
{
    /// <summary>
    /// Summary description for SimpleSenderTest
    /// 
    /// Note that this test class uses a PrivateType object to wrap up a SimpleSender object
    /// so the test cases can call the private methods
    /// </summary>
    [TestClass]
    public class SimpleSenderTest
    {
        [TestClass]
        public class SimpleSenderTester
        {
            private static readonly object MyLock = new object();

            [TestMethod]
            public void SimpleSenderTester_TestSingleGetNextId()
            {
                var senderWrapper = new PrivateObject(new SimpleSender());
                var id1 = (int) senderWrapper.Invoke("GetNextId");
                var id2 = (int) senderWrapper.Invoke("GetNextId");
                if (id1 != int.MaxValue)
                    Assert.IsTrue(id2 == id1 + 1);
                else
                    Assert.AreEqual(1, id2);
            }

            [TestMethod]
            public void SimpleSenderTester_TestLotsOfGetNextId()
            {
                var simpleSenderWrapper = new PrivateObject(new SimpleSender());
                Parallel.For(0, 1000, i =>
                {
                    var id1 = (int) simpleSenderWrapper.Invoke("GetNextId");
                    var id2 = (int) simpleSenderWrapper.Invoke("GetNextId");
                    Assert.IsTrue(id2 > id1);
                });
            }

            [TestMethod]
            public void SimpleSenderTester_TestRolloverOfGetNextId()
            {
                var simpleSenderWrapper = new PrivateObject(new SimpleSender());
                simpleSenderWrapper.SetField("_nextId", int.MaxValue);
                var id = (int) simpleSenderWrapper.Invoke("GetNextId");
                Assert.AreEqual(1, id);
            }

            [TestMethod]
            public void SimpleSenderTester_TestSendToPeers()
            {
                var sender = new SimpleSender();
                var senderWrapper = new PrivateObject(sender);
                var peers = senderWrapper.GetField("_peers") as List<IPEndPoint>;
                Assert.IsNotNull(peers);

                var clients = new List<UdpClient>();

                for (var i = 0; i < 100; i++)
                {
                    var client = CreateReceiver();
                    clients.Add(client);
                    peers.Add(GetLocalIpEndPoint(client));
                }

                var beginTime = DateTime.Now;

                lock (MyLock)
                {
                    senderWrapper.Invoke("SendToPeers", "Test message");
                }                    

                Parallel.ForEach(clients, client =>
                {
                    var incomingMessage = ReceiveMessage(client);
                    Assert.IsNotNull(incomingMessage);
                    Assert.IsTrue(beginTime < incomingMessage.Timestamp);
                    Assert.AreEqual("Test message", incomingMessage.Text);
                });
            }

            private UdpClient CreateReceiver()
            {
                var localEp = new IPEndPoint(IPAddress.Any, 0);
                var client = new UdpClient(localEp) { Client = { ReceiveTimeout = 5000 }};

                return client;
            }

            private static IPEndPoint GetLocalIpEndPoint(UdpClient client)
            {
                return new IPEndPoint(IPAddress.Loopback, ((IPEndPoint) client.Client.LocalEndPoint).Port);
            }

            private static Message ReceiveMessage(UdpClient client)
            {
                var remoteEp = new IPEndPoint(IPAddress.Any, 0);
                byte[] bytes = null;

                try
                {
                    bytes = client.Receive(ref remoteEp);
                }
                catch (SocketException err)
                {
                    if (err.SocketErrorCode != SocketError.TimedOut)
                        throw;
                }

                var result = Message.Decode(bytes);
                return result;
            }
        }
    }
}
